<img src="images/entete/head.png">

Auteur : Framatophe

Copyright : mars 2017

Licence: [Art Libre](http://artlibre.org/)

<a href="https://liberapay.com/Framatophe/donate"><img src="images/lpdonner.png" width="80px"></a>

-------


# Introduction

Trop compliqué&nbsp;! pas l'temps… ultra-sollicités entre nos communications, nos applications et nos appareils nous n'avons paradoxalement guère le temps de nous interroger sur la pertinence de nos besoins logiciels, leurs configurations, la sécurité de nos données et, au-delà, sur nos pratiques numériques. 

Mais qui prend ce temps aujourd'hui&nbsp;? Nous devons tous plus ou moins faire confiance aux éditeurs de logiciels. Tout se passe comme si la réflexion sur les usages était la prérogative des passionnés d'informatiques, ces hackers venus d'un monde alternatif, ces voisins bienveillants chez qui tout le monde vient chercher la solution à ses petits ennuis d'ordinateur. 

Dans ce monde, jusqu'à une époque récente, les discussions allaient bon train, reprenant bien souvent avec moquerie les mésaventures logicielles de Madame Michu. Or, lassée des réflexions machistes dont elle faisait l'objet, Françoise Michu a pris sa retraite bien méritée. C'est désormais la famille Dupuis-Morizeau (qu'on abrégera en DuMo) qui se porte  volontaire pour supporter ces geeks qui prétendent penser à la place des autres. Bien plus informés que Madame Michu, habitués des environnements informatiques en tout genre, les DuMo assumeront ce nouveau rôle dans un domaine où les caricatures sont nombreuses, entre la «&nbsp;génération Y&nbsp;», la secrétaire perdue, et les grands-pères ultra-connectés.

Qui sont les DuMo&nbsp;? une famille avec un pouvoir d'achat moyen (mais solide), récemment abonnée à la fibre dans leur petit pavillon d'un village normand[^1]. Les comptes de messagerie des membres de la famille sont chez Google, gérés de main de maître par le grand fils, lycéen,  dont le profil Facebook possède plus de 100 followers, une fierté. La petite, elle, vient de passer au collège le Brevet Informatique et Internet aux normes européennes [Digcomp](https://ec.europa.eu/jrc/en/digcomp/digital-competence-framework), et sait parfaitement maîtriser toute la suite bureautique de Microsoft. Madame, elle, a suivi consciencieusement les formations qui lui étaient offertes sur Powerpoint et envoie régulièrement à ses contacts de magnifiques diaporamas avec des couchers de soleil mauves. Quant à Monsieur, il maintient un blog, gère les comptes bancaires en ligne, mais se plaint souvent de la lenteur de l'ordinateur familial et songe sérieusement à en acheter un nouveau.

Caricature&nbsp;? Oui… mais pas parce que les DuMo sont des consommateurs de services. Le problème, les DuMo le connaissent et en sont parfaitement conscients&nbsp;: leur méconnaissance d'Internet, l'opacité à leurs yeux des mécanismes à l'œuvre sur les serveurs que fréquentent leurs machines (ordinateur, smartphones et tablette) lorsqu'ils utilisent des services web, leurs difficultés à remédier aux «&nbsp;pannes&nbsp;» qu'ils rencontrent régulièrement, les limites de leurs savoirs-faire lorsqu'il faut utiliser des logiciels pour opérer quelques opérations plus complexes que d'habitude, les abandons frustrants devant l'apparente complexité de la configuration de leur système d'exploitation ou des logiciels de messagerie… tout cela leur fait sérieusement douter de leur autonomie numérique.



Jamais Internet n'a été autant source d'inquiétudes et de tensions.  On ne compte plus les ouvrages sur le loup (ou l'ogre) Google, sur les «&nbsp;dangers d'Internet&nbsp;», sur les risques liés à la confidentialité des données personnelles, sur la surveillance numérique des individus, sur la surveillance globale des populations… Tout cela est à la fois anxiogène et, en même temps, très peu d'ouvrages proposent des solutions concrètes[^2], accessibles à tous et facilement, pour affronter ce monde plein d'embûches.


Mais comment comprendre ces enjeux si, d'un autre côté, les outils et les composants de l'environnement numérique restent des boîtes noires&nbsp;? L'utilisation de logiciels libres, ceux que l'on peut partager et que la communauté peut expertiser et améliorer, est un premier pas vers l'autonomie numérique, une approche raisonnée et sereine de l'informatique et des réseaux. Or les logiciels ne font pas tout. Les pratiques des utilisateurs, en connaissance de cause, sont les éléments centraux d'un autre monde numérique, moins anxiogène et plus durable, éthique et solidaire.

Cet ouvrage s'adresse donc aux DuMo. Il est à la fois un manuel et un livre de conseils pratiques. L'objectif est de permettre au lecteur de différencier les bonnes pratiques de celles qui ne sont pas adaptées à ses besoins (et faussent ses  apprentissages en matière de logiciels) ou mettent en danger son intimité numérique (par exemple dans le domaine de la sécurité ou de l'usage des services web). Certaines sections pourront parfois paraître basiques, elles recèlent toujours une parcelle méconnue du sujet traité. Chaque chapitre considère un pilier des activités numériques, il en dessine les contours, soulève les enjeux et propose des solutions de logiciels libres dont la légitimité s'impose d'elle-même. Il est temps en effet, de formaliser quelques pratiques d'auto-défense numérique tout en permettant aux utilisateurs de s'approprier ces usages et ces outils, même s'ils ne sont pas libristes ou d'irréductibles activistes des libertés numériques. 

Le deviendront-ils&nbsp;? Ce n'est pas important. L'essentiel est qu'en essaimant les bonnes pratiques et les solutions logicielles alternatives, c'est vers un modèle d'environnement numérique serein et plus démocratique que nous nous dirigeons. C'est pourquoi ce livre a aussi une ambition d'éducation populaire&nbsp;: il est placé sous licence libre, ce qui permet, dans le respect du droit d'auteur, de se l'approprier, de le modifier et de redistribuer des versions modifiées, enrichies de l'expérience et/ou à destination d'autres lecteurs. Emparez vous-en&nbsp;!


## Remerciements

Je tiens ici à remercier toutes celles et ceux sans qui ce petit guide n'aurait pas vu le jour. Ils m'ont aussi donné l'opportunité d'acquérir les quelques connaissances qui me permettent de passer pour le spécialiste que je ne suis pas.

* Ma chère épouse, relectrice avisée, patiente et compréhensive…
* Mon cher Goofy, pour sa relecture attentive…
* Mes chers amis de Framasoft…
* Toute la communauté libriste francophone, et quelques correspondants avisés, tels Thelvaën Mandel, fmr, Tuxman…



[^1]: La famille DuMo est un levier scénaristique. Elle *n'existe pas*, ni en réalité (ou alors toute ressemblance serait vraiment fortuite…), ni même en théorie, car on se doute bien que dans ces conditions qui respirent la guimauve et l'ennui, les choses pourraient vite mal tourner, entre l'alcool, la drogue et le sexe, au grand bénéfice de la presse à sensation.

[^2]: Il y en a au moins un, écrit par Tristan Nitot, *Surveillance:// Les libertés au défi du numérique&nbsp;: comprendre et agir*, C&F Éditions, Caen, 2016.


